﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ApplicationCore.Exceptions
{
   class BookNotFoundException : Exception
   {
      public BookNotFoundException(int bookId)
         : base($"No book found with id {bookId}")
      {
      }

      protected BookNotFoundException(SerializationInfo info, StreamingContext context)
         : base(info, context)
      {
      }

      public BookNotFoundException(string message)
         : base(message)
      {
      }

      public BookNotFoundException(string message, Exception innerException)
         : base(message, innerException)
      {
      }
   }
}
