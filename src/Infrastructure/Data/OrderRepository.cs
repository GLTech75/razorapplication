﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ApplicationCore.Entities.OrderAggregate;
using ApplicationCore.Interfaces;

namespace Infrastructure.Data
{
   public class OrderRepository : IOrderRepository
   {
      public Order Add(Order entity)
      {
         throw new System.NotImplementedException();
      }

      public Task<Order> AddAsync(Order entity)
      {
         throw new System.NotImplementedException();
      }

      public void Delete(Order entity)
      {
         throw new System.NotImplementedException();
      }

      public Task DeleteAsync(Order entity)
      {
         throw new System.NotImplementedException();
      }

      public Order GetById(int id)
      {
         throw new System.NotImplementedException();
      }

      public Task<Order> GetByIdAsync(int id)
      {
         throw new System.NotImplementedException();
      }

      public Order GetByIdWithItems(int id)
      {
         throw new System.NotImplementedException();
      }

      public Task<Order> GetByIdWithItemsAsync(int id)
      {
         throw new System.NotImplementedException();
      }

      public Order GetSingleBySpec(ISpecification<Order> spec)
      {
         throw new System.NotImplementedException();
      }

      public IEnumerable<Order> List(ISpecification<Order> spec)
      {
         throw new System.NotImplementedException();
      }

      public IEnumerable<Order> ListAll()
      {
         throw new System.NotImplementedException();
      }

      public Task<List<Order>> ListAllAsync()
      {
         throw new System.NotImplementedException();
      }

      public Task<List<Order>> ListAsync(ISpecification<Order> spec)
      {
         throw new System.NotImplementedException();
      }

      public void Update(Order entity)
      {
         throw new System.NotImplementedException();
      }

      public Task UpdateAsync(Order entity)
      {
         throw new System.NotImplementedException();
      }
   }
}
